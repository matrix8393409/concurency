
#include <iostream>
#include <thread>
#include <vector>
#include <mutex>

void calculateSum(const std::vector<int>& arr, size_t start, size_t end, int& result) {
    int localSum = 0;

    for (size_t i = start; i < end; ++i) {
        localSum += arr[i];
    }

    result += localSum;
}

int main() {
    const size_t arraySize = 1000000;
    std::vector<int> dataArray(arraySize, 1); 

    int sum = 0;

    std::thread thread1([&]() { calculateSum(dataArray, 0, arraySize / 2, sum); });
    std::thread thread2([&]() { calculateSum(dataArray, arraySize / 2, arraySize, sum); });

    thread1.join();
    thread2.join();

    std::cout << "Total Sum: " << sum << std::endl;

    return 0;
}
